# README #

1. Fork this repository
2. Measure your working time.

# Exercise description #

Create a Qt mobile application with C++ and QML (qtquick2). The application has to be run on iOS and Android. When you implement your solution focus to use Qt and QML built-in functionalities and classes. 

You have to create a C++ 'engine' side of the application where all of the data collection and data handling happens. The data has to come from a WebSocket connection.  When the user starts the application right in the startup process, the app has to build up the connection without any user interaction. 

WebSocket URL:  ws://seamantec-assessment.herokuapp.com/websocket

After the connection, the app has to subscribe to PositionChannel. 

```
#!javascript
{"command": "subscribe","identifier":"{\"channel\":\"PositionChannel\"}"}
```
After the subscription the server is starting push data to the application. 

Sample data:
```
#!javascript

{"identifier":"{\"channel\":\"PositionChannel\"}","message":{"line_count":2588,"heading":358.0}}
{"identifier":"{\"channel\":\"PositionChannel\"}","message":{"line_count":2607,"latitude":44.94245,"longitude":14.587666666666667}}
```

The engine side has to collect all positions and store them in a List. We will use this list of positions to draw a track line on a map. If there is more than one map in the application all has to use this one list to draw a polyline. 

If the incoming data type is a heading, the app just has to store the actual data and let the property bindable. 

## UI ##
![assessment.png](https://bitbucket.org/repo/KEabnq/images/1727457340-assessment.png)
The UI has to be able to handle two separate map object in a swipe view. The map has to be a fully functional zoomable and draggable map. Use any type of map what you like eg. Mapbox. 

To switch between views the user can use the down gray toolbar area. 

On both map has to draw the track log from the collected positions and rotate the boat to the heading direction. Bind the heading data with the heading label. 

Use this small boat image in the application. 
![map_HDG.png](https://bitbucket.org/repo/KEabnq/images/3836934556-map_HDG.png)